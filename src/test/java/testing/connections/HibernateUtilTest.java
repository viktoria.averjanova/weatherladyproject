package testing.connections;

import Functions.Location;
import Utilities.TestingHibernateUtilities;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.jupiter.api.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

// User HSQLDB for in-memory DB testing
// See https://examples.javacodegeeks.com/core-java/junit/junit-hsqldb-example/

public class HibernateUtilTest {

    private static SessionFactory sessionFactory;
    protected Session session;

    @BeforeAll
    public static void setup() throws SQLException, ClassNotFoundException, IOException {

        // Check whether such class can be loaded at all in runtime
        assertNotNull( Class.forName("org.hsqldb.jdbc.JDBCDriver") );
        sessionFactory = TestingHibernateUtilities.getSessionFactory(Location.class);

        assertNotNull( sessionFactory, "Session factory initialization fails" );
        assertFalse( sessionFactory.isClosed(), "Session cannot be opened" );
        System.out.println("SessionFactory created");
    }

    @AfterAll
    public static void tearDown() {
        if (sessionFactory != null) sessionFactory.close();
        System.out.println("SessionFactory destroyed");
    }

    @Test
    public void testCreate() {
        Transaction tx = session.beginTransaction();
        Location location = new Location("Tallinn", "Harjumaa", "Estonia");
        session.persist(location);
        assertNotNull(location.getId());
        tx.commit();
    }

    @Test
    public void testUpdate() {
        Transaction tx = session.beginTransaction();
        Location location = new Location("Tallinn", "Harjumaa", "Estonia");
        session.persist(location);
        tx.commit();

        tx = session.beginTransaction();
        location.setRegion("Harju");
        session.merge(location);
        tx.commit();

        tx = session.beginTransaction();
        Location updatedLocation = session.get(Location.class, location.getId());
        assertEquals("Harju", updatedLocation.getRegion());
        tx.commit();
    }

    @Test
    public void testGet() {
        Transaction tx = session.beginTransaction();
        Location location = new Location("Tallinn", "Harjumaa", "Estonia");
        session.persist(location);
        tx.commit();

        tx = session.beginTransaction();
        Location retrievedLocation = session.get(Location.class, location.getId());
        assertNotNull(retrievedLocation);
        assertEquals("Tallinn", retrievedLocation.getCity());
        assertEquals("Harjumaa", retrievedLocation.getRegion());
        assertEquals("Estonia", retrievedLocation.getCountry());
        tx.commit();
    }

    @Test
    public void testList() {
        Transaction tx = session.beginTransaction();
        session.persist(new Location("Tallinn", "Harjumaa", "Estonia"));
        session.persist(new Location("Riga", "Riga", "Latvia"));
        session.persist(new Location("Vilnius", "Vilnius", "Lithuania"));
        tx.commit();

        tx = session.beginTransaction();
        List<Location> locations = session.createQuery("FROM Location").list();
        assertEquals(4, locations.size());
        tx.commit();
    }

    @Test
    public void testDelete() {
        Transaction tx = session.beginTransaction();
        Location location = new Location("Los Angeles", "California", "USA");
        session.persist(location);
        tx.commit();

        tx = session.beginTransaction();
        session.delete(location);
        tx.commit();

        tx = session.beginTransaction();
        Location deletedLocation = session.get(Location.class, location.getId());
        assertNull(deletedLocation);
        tx.commit();
    }

    @BeforeEach
    public void openSession() {
        session = sessionFactory.openSession();
        System.out.println("Session created");
    }

    @AfterEach
    public void closeSession() {
        if (session != null) session.close();
        System.out.println("Session closed\n");
    }
}