package testing.dao;

import java.sql.SQLException;

import Functions.Location;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import testing.connections.HibernateUtilTest;

import static org.junit.jupiter.api.Assertions.*;

public class LocationDAOTest extends HibernateUtilTest {

    private LocationDAO locationDAO;

    @BeforeEach
    public void openSession() {

        System.out.println("Session created:" + super.session );
    }

    @AfterEach
    public void closeSession() {
        if (session != null) session.close();
        System.out.println("Session closed:" + super.session );
    }

    @Test
    public void testValidLocation() throws SQLException {

        Location location = new Location("Tallinn", "Harju", "Estonia");

        assertEquals("Tallinn", location.getCity());
        assertEquals("Harju", location.getRegion());
        assertEquals("Estonia", location.getCountry());

        // assertNotNull( super.session, "Session is not initialized!" );

    }/*

    @Test
    public void testGetAllLocations() throws SQLException {
        List<Location> locations = locationDAO.getAll();
        assertNotNull(locations);
        assertTrue(locations.size() > 0);

    }*/
}