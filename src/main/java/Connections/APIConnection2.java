package Connections;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

// Documentation: https://openweathermap.org/current
public class APIConnection2 {
    public static StringBuffer getWeatherData(String location) throws Exception {
        String apiKey = "64f878d6a084c5e79fb3337a46b640d8";
        URL url = new URL("https://api.openweathermap.org/data/2.5/weather?q=" + location + "&appid=" + apiKey + "&units=metric");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        con.disconnect();

        return response;
    }
}

// JSON string received example:
// {"coord":{"lon":24,"lat":14},"weather":[{"id":802,"main":"Clouds","description":"scattered clouds","icon":"03n"}],"base":"stations","main":{"temp":302.8,"feels_like":301.01,"temp_min":302.8,"temp_max":302.8,"pressure":1008,"humidity":11,"sea_level":1008,"grnd_level":900},"visibility":10000,"wind":{"speed":3.53,"deg":120,"gust":4.23},"clouds":{"all":35},"dt":1682873655,"sys":{"country":"SD","sunrise":1682827360,"sunset":1682872780},"timezone":7200,"id":371745,"name":"Kutum","cod":200}