package Connections;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

// Documentation: https://weatherstack.com/documentation
public class APIConnection1 {

    public static StringBuffer getWeatherData(String location) throws Exception {
        String apiKey = "59048b85d99f048fe8e3dc08ef6e4fa1";
        URL url = new URL("http://api.weatherstack.com/current?access_key=" + apiKey + "&query=" + location);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        con.disconnect();

        return response;
    }
}

//JSON String example:
// {"request":{"type":"City","query":"Tallinn, Estonia","language":"en","unit":"m"},"location":{"name":"Tallinn","country":"Estonia","region":"Harjumaa","lat":"59.434","lon":"24.728","timezone_id":"Europe\/Tallinn","localtime":"2023-04-30 20:08","localtime_epoch":1682885280,"utc_offset":"3.0"},"current":{"observation_time":"05:08 PM","temperature":4,"weather_code":296,"weather_icons":["https:\/\/cdn.worldweatheronline.com\/images\/wsymbols01_png_64\/wsymbol_0017_cloudy_with_light_rain.png"],"weather_descriptions":["Light Rain Shower"],"wind_speed":24,"wind_degree":270,"wind_dir":"W","pressure":1012,"precip":0,"humidity":87,"cloudcover":75,"feelslike":-1,"uv_index":2,"visibility":10,"is_day":"yes"}}