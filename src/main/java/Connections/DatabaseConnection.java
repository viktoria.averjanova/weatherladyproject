package Connections;

import java.sql.*;

public class DatabaseConnection {
    public static void main(String[] args) {
        Connection conn = null;
        try {
            // Load the MySQL JDBC driver
            Class.forName("com.mysql.cj.jdbc.Driver");

            // Establish a connection
            String url = "jdbc:mysql://localhost:3306/weatherlady";
            String username = "root";
            String password = "0000";
            conn = DriverManager.getConnection(url, username, password);

            // Test the connection
            if (conn.isValid(5)) {
                System.out.println("Connection is valid.");
            } else {
                System.out.println("Connection is invalid.");
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // Close the connection
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
