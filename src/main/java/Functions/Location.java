package Functions;

import jakarta.persistence.*;

@Entity
@Table(name = "locations")
public class Location {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "city_name", nullable = false)
    private String city;

    @Column(name = "date", unique = true)
    private String date;

    @Column(name = "longitude")
    private float lon;

    @Column(name = "latitude")
    private float lat;

    @Column(name = "region")
    private String region;

    @Column(name = "country_name", nullable = false)
    private String country;

    @Column(name = "temperature")
    private Float temp;

    @Column(name = "pressure")
    private Float pressure;

    @Column(name = "humidity")
    private Float humidity;

    @Column(name = "wind_direction")
    private String windDir;

    @Column(name = "wind_speed")
    private Float windS;

    protected Location() {

    }

    public Location(String city, String region, String country) {

        this.city = city;
        this.region = region;
        this.country = country;
    }


    public int getId() {
        return id;
    }

    public String getCity() {
        return city;
    }

    public String getDate() {
        return date;
    }

    public float getLon() {
        return lon;
    }

    public float getLat() {
        return lat;
    }

    public String getRegion() {
        return region;
    }

    public String getCountry() {
        return country;
    }

    public Float getTemp() {
        return temp;
    }

    public Float getPressure() {
        return pressure;
    }

    public Float getHumidity() {
        return humidity;
    }

    public String getWindDir() {
        return windDir;
    }

    public Float getWindS() {
        return windS;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setLon(float lon) {
        this.lon = lon;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setTemp(Float temp) {
        this.temp = temp;
    }

    public void setPressure(Float pressure) {
        this.pressure = pressure;
    }

    public void setHumidity(Float humidity) {
        this.humidity = humidity;
    }

    public void setWindDir(String windDir) {
        this.windDir = windDir;
    }

    public void setWindS(Float windS) {
        this.windS = windS;
    }
}
