package Functions;

import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

import jakarta.persistence.*;
import java.util.List;

public class DisplayTable {
    public static void displayTable() {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("weatherlady");
        EntityManager em = emf.createEntityManager();

        TypedQuery<Location> query = em.createQuery("SELECT l FROM Functions.Location l", Location.class);
        List<Location> locations = query.getResultList();
        System.out.println(String.format("%15S","id | cityName | longitude | latitude | region | countryName"));
        for (Location location : locations) {
            System.out.println(String.format("%15s", location.getId() + " | " + location.getCity() + " | " + location.getLat() + "| " + location.getLon() + " | " + location.getRegion() + " | " + location.getCountry()));
        }

        em.close();
        emf.close();
    }
}
