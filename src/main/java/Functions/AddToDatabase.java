package Functions;

import Connections.APIConnection1;
import Connections.APIConnection2;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AddToDatabase {
    public static void addCity(String inputCity) throws Exception {

        EntityManagerFactory entityManagerFactory = null;
        EntityManager entityManager = null;

        Location newLocation = new Location();

        //API connection1
        String jsonString1 = String.valueOf(APIConnection1.getWeatherData(inputCity));
        ObjectMapper mapper1 = new ObjectMapper();
        JsonNode jsonNode1 = mapper1.readTree(jsonString1);
        String city = jsonNode1.get("location").get("name").asText();
        String lon = jsonNode1.get("location").get("lon").asText();
        String lat = jsonNode1.get("location").get("lat").asText();
        String region = jsonNode1.get("location").get("region").asText();
        String country = jsonNode1.get("location").get("country").asText();
        int temperature1 = jsonNode1.get("current").get("temperature").asInt();
        int pressure1 = jsonNode1.get("current").get("pressure").asInt();
        int humidity1 = jsonNode1.get("current").get("humidity").asInt();
        String windDir = jsonNode1.get("current").get("wind_dir").asText();
        int windSpeed1 = jsonNode1.get("current").get("wind_speed").asInt();


        //API connection2
        String jsonString2 = String.valueOf(APIConnection2.getWeatherData(inputCity));
        ObjectMapper mapper2 = new ObjectMapper();
        JsonNode jsonNode2 = mapper2.readTree(jsonString2);
        int temperature2 = jsonNode2.get("main").get("temp").asInt();
        int pressure2 = jsonNode2.get("main").get("pressure").asInt();
        int humidity2 = jsonNode2.get("main").get("humidity").asInt();
        int windSpeed2 = jsonNode2.get("wind").get("speed").asInt();
        //To retrieve data dd-MM-yyyy format
        long unixTimestamp = jsonNode2.get("dt").asLong();
        Date rawDate = new Date(unixTimestamp * 1000L); // Convert Unix timestamp to Java Date object
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // Set the desired date format
        String date = sdf.format(rawDate); // Convert Date object to formatted date string


        //Average of APIs values;
        double temp = (temperature1 + temperature2) / 2;
        float pressure = (pressure1 + pressure2) / 2;
        float humidity = (humidity1 + humidity2) / 2;
        float windSpeed = (windSpeed1 + windSpeed2) / 2;


        //Save average data to database
        try {
            entityManagerFactory = Persistence.createEntityManagerFactory("weatherlady");
            entityManager = entityManagerFactory.createEntityManager();
            entityManager.getTransaction().begin();

            newLocation.setCity(city);
            newLocation.setDate(date);
            newLocation.setLon(Float.parseFloat(lon));
            newLocation.setLat(Float.parseFloat(lat));
            newLocation.setRegion(region);
            newLocation.setCountry(country);
            newLocation.setTemp((float) temp);
            newLocation.setPressure(pressure);
            newLocation.setHumidity(humidity);
            newLocation.setWindDir(windDir);
            newLocation.setWindS(windSpeed);

            entityManager.persist(newLocation);
            entityManager.getTransaction().commit();
            System.out.println(city + "(Lon: " + lon + ", Lat " + lat + "), " + region + ", " + country + " - was added to database successfully!");

            entityManager.persist(newLocation);
            entityManager.getTransaction().commit();
            System.out.println("Location added successfully!");
        } catch (Exception e) {

        } finally {
            if (entityManager != null) {
                entityManager.close();
            }
            if (entityManagerFactory != null) {
                entityManagerFactory.close();
            }
        }
    }
}
