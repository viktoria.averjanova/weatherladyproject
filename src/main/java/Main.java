import Connections.APIConnection1;
import Functions.AddToDatabase;
import Functions.DisplayTable;
import Functions.ReadJSON;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws Exception {
        String instruction = "\n" +
                "--------------------------------\n" +
                "Type:\n" +
                "1 - To add location to database\n" +
                "2 - To display added locations\n" +
                "3 - To see current weather\n" +
                "4 - To exit\n" +
                "--------------------------------";
        System.out.println(instruction);

        Scanner scn = new Scanner(System.in);
        Integer input;

        while (!scn.hasNextInt()) {
            System.out.println("Please enter correct number: ");
            scn = new Scanner(System.in);
        }
        input = scn.nextInt();

        do {
            switch (input) {
                case 1:
                    System.out.println("Enter location:");
                    Scanner scanner = new Scanner(System.in);
                    String inputCity = scanner.next();

                    AddToDatabase add = new AddToDatabase();
                    add.addCity(inputCity);
                    break;

                case 2:
                    DisplayTable table = new DisplayTable();
                    table.displayTable();
                    break;

                case 3:
                    System.out.println("Enter location:");
                    Scanner scn2 = new Scanner(System.in);
                    String inputLocation = scn2.nextLine();
                    String jsonString = String.valueOf(APIConnection1.getWeatherData(inputLocation));
                    ReadJSON forecast = new ReadJSON();
                    forecast.seeForecast(jsonString);
                    break;

                case 4:
                    System.out.println("4");
                    break;
                default:
                    System.out.println("Number out of list. Bye");
            }
            System.out.println(instruction);
            input = scn.nextInt();
        }
        while (input != 4);
    }
}

