CREATE DATABASE weatherlady;

CREATE TABLE locations (
    id INT AUTO_INCREMENT PRIMARY KEY,
    city_name VARCHAR(50) NOT NULL,
    date DATETIME,
    longitude FLOAT,
    latitude FLOAT,
    region VARCHAR(50),
    country_name VARCHAR(50) NOT NULL,
    temperature FLOAT,
    pressure FLOAT,
    humidity FLOAT,
    wind_direction VARCHAR(50),
    wind_speed FLOAT
);